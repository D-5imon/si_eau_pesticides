# L'application

Cette application met à disposition les données sur les pesticides dans les cours d'eau de la région des Pays de la Loire à partir de 2011.

Ces données sont issues de l'Agence de l'eau Loire Bretagne, de l'Agence régionale de santé et du réseau complémentaire de suivi régional financé par la DREAL, la DRAAF et l'Agence de l'Eau. 

Ces données seront mises à jour régulièrement et l'application enrichie au fur et à mesure.

N'hésitez pas à [nous contacter](mailto:srnp.dreal-paysdelaloire@developpement-durable.gouv.fr?subject=Application pesticides&body=Bonjour,") pour toute remarque ou suggestion.

