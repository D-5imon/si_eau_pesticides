# La donnée : des mesures de pesticides dans les cours d’eau

Le terme « pesticides » désigne les substances actives utilisées pour la prévention, le contrôle ou l’élimination d’organismes jugés indésirables (plantes, animaux, champignons, bactéries…). Ce terme englobe ainsi les produits phytopharmaceutiques,  utilisées pour un usage agricole, domestique ou par les collectivités (appelés aussi produits phytosanitaire), mais également les produits dits « biocides » utilisés dans les secteurs professionnels non agricoles ou dans un cadre domestique, ainsi que des antiparasitaires à usage humain ou vétérinaires.

Ainsi, selon les années, du fait de l’évolution des usages d’un côté, et de l’amélioration des techniques d’analyse par les laboratoires de l’autre, les molécules recherchées et retrouvées peuvent différer, et être associées à une catégorie ou plusieurs catégories évoquées ci-dessous.
Dans tous les cas, leur présence pose un certain nombre de questionnements quant à leur origine, leur vitesse de dégradation dans le milieu, leur usage.

La présente application n’aborde pas cette problématique, elle livre les mesures de pesticides dans les cours d’eau sans chercher à établir des corrélations ou des liens de causalité sur l’usage des substances relevées par les mesures.

Les données concernent uniquement les eaux de surface, sur des stations de suivi ou des point de prélèvement destiné à l’eau potable. Les données des eaux souterraines ne sont pas encore intégrées.

Elles proviennent :

- des données [Naiades](http://www.naiades.eaufrance.fr/) présentes sur la plateforme [HUB’eau](https://hubeau.eaufrance.fr/), issues des programmes de surveillance de l’agence de l’eau et de toutes les autres données collectées par les collectivités dans le cadre des contrats territoriaux, programmes de suivi locaux, ...
- des données du programme de surveillance de l’ARS Pays de la Loire.

La plateforme HUB’eau est interrogée sur la base d’une liste de 951 molécules (disponible dans la rubrique « consulter les molécules ». Cette liste n’est pas exhaustive mais couvre déjà un nombre important de molécules relevant de cette catégorie. Elle peut bien sûr évoluer dans le temps. Certaines molécules pouvant rentrer dans cette catégorie mais ayant d’autres usages principaux n’ont pas été intégrés (PCB, méthanal,…).

*Attention ! Les stations prisent en compte pour les calculs sont celles qui se situent sur le territoire de la région des Pays de la Loire. Pour un SAGE à cheval sur les Pays de la Loire et une autre région, les mesures des stations de l’autre région ne participent pas aux calculs.*


        
    
