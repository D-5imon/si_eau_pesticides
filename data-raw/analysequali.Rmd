---
title: "Analyse qualité des données pesticides"
output: html_notebook
---

# Les prélèvements

Test pour voir si l'on trouve des prélèvements en double dans la table.
Un prélèvement est présumé en double si on retrouve pour deux prélèvements pour une station à une même date.

```{r,include=FALSE}
knitr::opts_chunk$set(echo = T,warning = F,error = F,message = F,
                      width=100)
```

```{r}
library(tidyverse)
library(DT)
prelevement_en_double<-prelevement %>% 
  select(date_prelevement,code_station,code_prelevement) %>% 
  group_by(date_prelevement,code_station) %>% 
  add_count() %>% 
  group_by(date_prelevement,code_station,n) %>% 
  nest() %>%
  filter(n>1)
```

`r nrow(prelevement_en_double)` prélèvements en double

# Les analyses

Test pour voir si l'on trouve des analyses en double dans la table. Une analyse est présumée en double si on retrouve pour un même prélèvements deux fois une molécule mesurée.


```{r}
analyse_en_double_avec_na<-analyse %>% 
  select(code_analyse,code_prelevement,code_parametre) %>% 
  group_by(code_prelevement,code_parametre) %>% 
  add_count() %>% 
  group_by(code_prelevement,code_parametre,n) %>% 
  nest() %>%
  filter(n>1)
```

`r nrow(analyse_en_double_avec_na)` analyses en double, soit `r round(100*nrow(analyse_en_double_avec_na)/nrow(analyse),1)` % des analyses

Beaucoup de ces analyses en double concerne des mesures avec des codes paramètre à vide. Le fait de retrouver plusieurs valeurs manquantes pour le code_parametre n'est pas en soit un problème de doublon, mais un autre problème qualité.

```{r}
analyse_en_double<- analyse_en_double_avec_na %>%
  filter(!is.na(code_parametre))
```

En les supprimant,il reste toutefois `r nrow(analyse_en_double)` analyses en double sur des codes paramètres renseignés.

On regarde ce qui se passe si on ajoute le résultat de l'analyse comme critère

```{r}
analyse_en_double_avec_resultat<-analyse %>% 
  select(code_analyse,code_prelevement,code_parametre,resultat_analyse) %>% 
  group_by(code_prelevement,code_parametre,resultat_analyse) %>% 
  add_count() %>%
  filter(n>1,!is.na(code_parametre)) %>% 
  group_by(code_prelevement,code_parametre,resultat_analyse,n) %>% 
  nest()
```

`r nrow(analyse_en_double_avec_resultat)` analyses en double avec ce nouveau critère, soit `r round(100*nrow(analyse_en_double_avec_resultat)/nrow(analyse),1)` % des analyses. Cela signifie que nous avons `r nrow(analyse_en_double)-nrow(analyse_en_double_avec_resultat)` analyses réalisées en double lors d'un prélèvement avec des résultats différents, soit `r round(100*(nrow(analyse_en_double)-nrow(analyse_en_double_avec_resultat))/nrow(analyse),1)` % des analyses.

# Répartition des prélèvements ayant au moins une analyse en double par date.

On garde les prélèvements retrouvés dans la table analyse_en_double analyse_en_double_avec_resultat.

```{r}
prelevement_avec_analyse_en_double<-prelevement %>% 
  inner_join(analyse_en_double) %>% 
  select(date_prelevement,code_prelevement,source) %>% 
  distinct()

prelevement_avec_analyse_en_double_avec_resultat<-prelevement %>% 
  inner_join(analyse_en_double_avec_resultat) %>% 
  select(date_prelevement,code_prelevement,source) %>% 
  distinct()
```

Ces analyses se retrouvent respectivement dans `r nrow(prelevement_avec_analyse_en_double)` et `r nrow(prelevement_avec_analyse_en_double_avec_resultat)` prélèvements, soit `r round(100*nrow(prelevement_avec_analyse_en_double)/nrow(prelevement),1)` % et `r round(100*nrow(prelevement_avec_analyse_en_double_avec_resultat)/nrow(prelevement),1)` % des prélèvements. 

On ne garde donc maintenant que prelevement_avec_analyse_en_double et on regarde comme se répartissent ces prélèvements en fonction de leur date.

```{r}
ggplot(prelevement_avec_analyse_en_double,aes(date_prelevement,fill=source))+
  geom_histogram()+
  theme_minimal()+
  labs(title="Prélèvements avec analyses en double (méthode 1)",y="Nombre de prélèvements",x="Date de prélèvement")
```

L'immense majorité de ces prélèvements sont issus de l'ARS (`r round(100*nrow(prelevement_avec_analyse_en_double[which(prelevement_avec_analyse_en_double$source=="ARS"),])/nrow(prelevement_avec_analyse_en_double),1)` %)

Il sont assez concentré sur la période 2010-2012 avec un pic également en 2016.

Ces prélèvements représentent `r round(100*nrow(prelevement_avec_analyse_en_double[which(prelevement_avec_analyse_en_double$source=="ARS"),])/nrow(prelevement[which(prelevement$source=="ARS"),]),1)` % de l'ensemble des prélèvements de l'ARS.


```{r}
prelevement_ars_avec_pb<-prelevement %>% 
  mutate(double=ifelse(code_prelevement %in% prelevement_avec_analyse_en_double$code_prelevement,"Avec analyse en double","Sans analyse en double")) %>% 
  filter(source=="ARS")

ggplot(prelevement_ars_avec_pb,aes(date_prelevement,fill=double))+
  geom_histogram()+
  theme_minimal()+
  labs(title="Prélèvements ARS avec ou sans analyses en double (méthode 1)",y="Nombre de prélèvements",x="Date de prélèvement")

```

Si on regarde l'ensemble des prélèvements ARS en fonction de ces anomalies, on constate que ces prélèvements représentent une grand part des prélèvements de la période 2010-2012.

# Molécules concernées

```{r}
analyse %>% 
  filter(code_analyse %in% (analyse_en_double %>% 
                              unnest() %>% 
                              pull(code_analyse))) %>% 
  left_join(parametre %>% 
              select(code_parametre,nom_parametre)) %>% 
  group_by(nom_parametre) %>% 
  tally %>% 
  arrange(-n) %>% 
  mutate(nom_parametre=factor(nom_parametre) %>% fct_inorder()) %>% 
  slice(1:20) %>% 
  ggplot(aes(x=nom_parametre,weight=n))+
    geom_bar()+
    coord_flip()+
  labs(title="Les 20 principales molécules retrouvées analysées en double",x="molécule",y="nombre d'analyses")
```

# Exemple sur quelques cas

Ci dessous on sélectionne pour chaque station et date ou l'on retrouve des analyses en double 2 mesures en double.
```{r}
datatable(analyse %>% 
  filter(code_analyse %in% (analyse_en_double %>% unnest() %>% pull(code_analyse))) %>% 
  left_join(prelevement %>% select(code_prelevement,code_station,date_prelevement,source)) %>% 
  left_join(station %>% select(code_station,libelle_station)) %>% 
  select(source,code_station,libelle_station,date_prelevement,code_prelevement,date_prelevement,code_parametre,code_remarque,resultat_analyse) %>%
  arrange(code_station,date_prelevement,code_parametre) %>% 
  group_by(code_station,libelle_station,source,date_prelevement) %>% 
  slice(1:4) %>% 
  ungroup 
)
```

```{r echo=F}
write_excel_csv2(analyse %>% 
            filter(code_analyse %in% (analyse_en_double %>% unnest() %>% pull(code_analyse))) %>% 
            left_join(prelevement %>% select(code_prelevement,code_station,date_prelevement,code_reseau)) %>% 
            left_join(station %>% select(code_station,libelle_station)) %>% 
            arrange(code_station,date_prelevement,code_parametre) %>% 
            select(code_station,libelle_station,code_reseau,date_prelevement,code_parametre,code_remarque,resultat_analyse),
          "analyse_en_double.csv")
```
